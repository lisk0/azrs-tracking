# Azrs Tracking
Primena alata za razvoj softvera na projektu koji je radjen samostalno pre par godina na kursu "Razvoj softvera".

Projekat: https://github.com/MATF-RS20/RS031-vizualizacija-pathfinder-algoritama/

# Primenjeni alati

1. [CMake](https://gitlab.com/lisk0/azrs-tracking/-/issues/1)

2. [Clang Tidy](https://gitlab.com/lisk0/azrs-tracking/-/issues/2)

3. [Clazy](https://gitlab.com/lisk0/azrs-tracking/-/issues/4)

4. [GDB](https://gitlab.com/lisk0/azrs-tracking/-/issues/5)

5. [Gammaray](https://gitlab.com/lisk0/azrs-tracking/-/issues/7)

6. [Clang Format](https://gitlab.com/lisk0/azrs-tracking/-/issues/6)

7. [Memcheck](https://gitlab.com/lisk0/azrs-tracking/-/issues/9)

8. [Git](https://gitlab.com/lisk0/azrs-tracking/-/issues/8)
